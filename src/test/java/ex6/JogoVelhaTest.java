package ex6;

import exs1.ex6.velha.JogoVelha;
import org.junit.Test;

import java.util.Arrays;

public class JogoVelhaTest {

    @Test
    public void splitCoordenatesTest(){
        JogoVelha velha = new JogoVelha();
        String[] res = velha.splitCoordinates("( 3, 0)");
        String[] answer = {"3", "0"};

        assert Arrays.equals(res, answer);
    }
}

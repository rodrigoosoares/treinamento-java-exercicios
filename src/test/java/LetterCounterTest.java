import exs1.ex1.classesauxiliares.Parameters;
import exs1.ex2.classes.LetterCounter;
import exs1.ex2.classesauxiliares.IndividualParameter;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class LetterCounterTest {

    @Test
    public void test(){
        List<String> words = new ArrayList<>();
        LetterCounter counter = new LetterCounter();
        Parameters params = new Parameters(words);
        List<IndividualParameter> resposta = new ArrayList<>();
        List<IndividualParameter> result = new ArrayList<>();

        words.add("pala");
        result = counter.count(params);
        resposta.add(new IndividualParameter('p', 1));
        resposta.add(new IndividualParameter('a', 2));
        resposta.add(new IndividualParameter('l', 1));

        assert result.retainAll(resposta);
    }
}

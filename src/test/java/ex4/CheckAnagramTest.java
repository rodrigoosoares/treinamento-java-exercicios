package ex4;

import exs1.ex4.classes.CheckAnagram;
import exs1.ex4.classesauxiliares.Parameter;
import org.junit.Test;

public class CheckAnagramTest {

    @Test
    public void test(){
        CheckAnagram check = new CheckAnagram();
        Parameter params = new Parameter("Manda jogar em vapor", "Programando em java");
        boolean res = check.check(params);
        assert res;
    }
}

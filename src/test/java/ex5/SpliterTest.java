package ex5;

import exs1.ex5.classesauxiliares.Spliter;
import exs1.ex5.interfaces.IOperation;
import exs1.ex5.operations.Sum;
import org.junit.Test;

import java.util.Arrays;

public class SpliterTest {

    @Test
    public void splitParametersTest(){
        Spliter spliter = new Spliter();
        String[] res = spliter.splitParameters("1+8");
        String[] answer = {"1", "8"};
        assert Arrays.equals(res, answer);
    }

    @Test
    public void splitOperationTest(){
        Spliter spliter = new Spliter();
        IOperation res = spliter.splitOperation("1+8");
        assert res instanceof Sum;
    }
}

package exs1.ex3;

import exs1.ex3.classesauxiliares.NumberCheck;
import exs1.ex3.classesauxiliares.NumberGenerate;
import exs1.ex3.classesauxiliares.NumberInput;
import exs1.ex3.classesauxiliares.Parameters;

public class Main {
    public static void main(String[] args){
        System.out.println("Desafio [Acerte o número escolhido [1 ~ 100]");

        Executer executer = new Executer(new NumberCheck());
        executer.executeCheck(new Parameters(new NumberInput().input(), new NumberGenerate().getNum()));
    }
}

package exs1.ex3.interfaces;

import exs1.ex3.classesauxiliares.Parameters;

public interface ICheck {
    int check(Parameters params);
}

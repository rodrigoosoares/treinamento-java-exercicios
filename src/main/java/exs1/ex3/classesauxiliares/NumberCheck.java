package exs1.ex3.classesauxiliares;

import exs1.ex3.interfaces.ICheck;

public class NumberCheck implements ICheck {

    @Override
    public int check(Parameters params) {
        int typedNum = params.getTypedNum();
        int answrNum = params.getAnswrNum();

        if(typedNum < answrNum)
            return -1;
        else if(typedNum > answrNum)
            return 1;
        else
            return 0;
    }
}

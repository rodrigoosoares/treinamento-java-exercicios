package exs1.ex3.classesauxiliares;

public class Parameters {

    private int typedNum;
    private int answrNum;

    public Parameters(int typedNum, int answrNum) {
        this.typedNum = typedNum;
        this.answrNum = answrNum;
    }

    public int getTypedNum() {
        return typedNum;
    }

    public void setTypedNum(int typedNum) {
        this.typedNum = typedNum;
    }

    public int getAnswrNum() {
        return answrNum;
    }
}

package exs1.ex3.classesauxiliares;

public class ResultOutput {

    public static String resultOutput(int res){
        if(res < 0)
            return "O número digitado é menor que a resposta!";
        else if(res > 0)
            return "O número digitado é maior que a resposta!";
        else
            return "Parabêns, você acertou o número sorteado!";
    }
}

package exs1.ex3.classesauxiliares;

import java.util.Scanner;

public class NumberInput {

    public int input(){
        System.out.print("Digite um número: ");
        Scanner sc = new Scanner(System.in);
        return sc.nextInt();
    }
}

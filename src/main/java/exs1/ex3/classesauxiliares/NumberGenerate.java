package exs1.ex3.classesauxiliares;

import java.util.Random;

public class NumberGenerate {

    private int num;

    public NumberGenerate() {
        Random random = new Random();
        this.num = random.nextInt(100);
    }

    public int getNum() {
        return num;
    }
}

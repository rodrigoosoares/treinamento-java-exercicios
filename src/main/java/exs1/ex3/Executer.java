package exs1.ex3;

import exs1.ex3.classesauxiliares.NumberInput;
import exs1.ex3.classesauxiliares.Parameters;
import exs1.ex3.classesauxiliares.ResultOutput;
import exs1.ex3.interfaces.ICheck;

public class Executer {

    private ICheck check;

    public Executer(ICheck check) {
        this.check = check;
    }

    public void executeCheck(Parameters params){
        int res;
        while(true){
            res = check.check(params);
            System.out.println(ResultOutput.resultOutput(res));
            if(res == 0)
                break;
            params.setTypedNum(new NumberInput().input());
        }

    }
}

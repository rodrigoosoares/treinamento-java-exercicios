package exs1.ex4.classes;

import exs1.ex4.classesauxiliares.Parameter;
import exs1.ex4.interfaces.ICheck;

import java.util.Arrays;

public class CheckAnagram implements ICheck {
    @Override
    public boolean check(Parameter params) {
        String phrase1 = params.getPhrase1().toLowerCase().trim().replaceAll("[^A-Za-z]", "");
        String phrase2 = params.getPhrase2().toLowerCase().trim().replaceAll("[^A-Za-z]", "");
        //Phrase 1 sort
        char[] tempArray = phrase1.toCharArray();
        Arrays.sort(tempArray);
        phrase1 = new String(tempArray);

        //Phrase 2 sort
        tempArray = phrase2.toCharArray();
        Arrays.sort(tempArray);
        phrase2 = new String(tempArray);


        return phrase1.equals(phrase2);
    }
    /* Manda jogar em vapor
       Programando em java

       mandajogaremvapor
       programandoemjava
    */
}

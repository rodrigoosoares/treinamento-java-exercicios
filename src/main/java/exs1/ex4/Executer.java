package exs1.ex4;

import exs1.ex4.classesauxiliares.Parameter;
import exs1.ex4.interfaces.ICheck;

public class Executer {

    private ICheck check;

    public Executer(ICheck check) {
        this.check = check;
    }

    public void execute(Parameter params){
        boolean res = check.check(params);
        System.out.println("Resultado: " + res);

    }
}

package exs1.ex4;

import exs1.ex4.classes.CheckAnagram;
import exs1.ex4.classesauxiliares.PhrasesInput;

public class Main {

    public static void main(String[] args){
        System.out.println("Check de anagrama");

        Executer executer = new Executer(new CheckAnagram());
        executer.execute(new PhrasesInput().input());
    }
}

package exs1.ex4.interfaces;

import exs1.ex4.classesauxiliares.Parameter;

public interface ICheck {

    boolean check(Parameter params);
}

package exs1.ex4.classesauxiliares;

import java.util.Scanner;

public class PhrasesInput {

    public Parameter input(){
        String[] phrases = new String[2];
        Scanner sc = new Scanner(System.in);
        System.out.print("Digite a primeira frase: ");
        phrases[0] = sc.nextLine();
        System.out.print("Digite a segunda frase: ");
        phrases[1] = sc.nextLine();

        return new Parameter(phrases[0], phrases[1]);
    }
}

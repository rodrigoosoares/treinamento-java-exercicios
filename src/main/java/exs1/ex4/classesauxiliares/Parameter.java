package exs1.ex4.classesauxiliares;

public class Parameter {

    private String phrase1;
    private String phrase2;

    public Parameter(String phrase1, String phrase2) {
        this.phrase1 = phrase1;
        this.phrase2 = phrase2;
    }

    public String getPhrase1() {
        return phrase1;
    }

    public String getPhrase2() {
        return phrase2;
    }
}

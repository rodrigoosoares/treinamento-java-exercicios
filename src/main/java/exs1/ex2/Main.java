package exs1.ex2;

import exs1.ex1.classesauxiliares.Execute;
import exs1.ex1.classesauxiliares.Parameters;
import exs1.ex1.classesauxiliares.WordInput;
import exs1.ex1.interfaces.ICounter;
import exs1.ex2.classes.*;
import exs1.ex2.classesauxiliares.ExecuteIndividual;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args){
        WordInput wordInput = new WordInput();
        Parameters params = new Parameters(wordInput.input());
        List<ICounter> counters = new ArrayList<>();
        counters.add(new VowelCounter());
        counters.add(new ConsoantCounter());
        counters.add(new NumbersCounter());
        counters.add(new SpecialCharsCounter());

        for (ICounter counter : counters){
            Execute execute = new Execute(counter);
            execute.execute(params);
        }

        ExecuteIndividual executeIndividual = new ExecuteIndividual(new LetterCounter());
        executeIndividual.execute(params);

    }
}

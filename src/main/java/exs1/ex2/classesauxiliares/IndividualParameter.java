package exs1.ex2.classesauxiliares;

public class IndividualParameter {

    private char letter;
    private int count;

    public IndividualParameter(char letter, int count) {
        this.letter = letter;
        this.count = count;
    }

    public char getLetter() {
        return letter;
    }

    public void setLetter(char letter) {
        this.letter = letter;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "Letter: '" + letter + "' -> " + count + " times";
    }
}

package exs1.ex2.classesauxiliares;

public class Vowels {
    private static final char[] voewls = {'a', 'e', 'i', 'o', 'u'};

    public static char[] getVoewls() {
        return voewls;
    }

    public static char getVoewlAt(int pos){
        return voewls[pos];
    }

    public static int getVoewlsLength(){
        return voewls.length;
    }
}

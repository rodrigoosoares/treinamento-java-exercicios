package exs1.ex2.classesauxiliares;

import exs1.ex1.classesauxiliares.Parameters;
import exs1.ex2.interfaces.ICounterIndividual;

import java.util.List;

public class ExecuteIndividual {

    private ICounterIndividual counter;

    public ExecuteIndividual(ICounterIndividual counter) {
        this.counter = counter;
    }

    public void execute(Parameters params){
        List<IndividualParameter> res = counter.count(params);
        System.out.println(res.toString());
    }
}

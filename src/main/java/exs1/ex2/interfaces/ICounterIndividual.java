package exs1.ex2.interfaces;

import exs1.ex1.classesauxiliares.Parameters;
import exs1.ex2.classesauxiliares.IndividualParameter;

import java.util.List;

public interface ICounterIndividual<T> {

    List<T> count(Parameters params);
}

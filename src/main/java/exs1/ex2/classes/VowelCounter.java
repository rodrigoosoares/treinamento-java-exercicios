package exs1.ex2.classes;

import exs1.ex1.classesauxiliares.Parameters;
import exs1.ex1.interfaces.ICounter;
import exs1.ex2.classesauxiliares.Vowels;

import java.util.List;

public class VowelCounter implements ICounter {
    @Override
    public int count(Parameters params) {
        int count = 0;
        List<String> words = params.getWords();
        for (String word : words){
            for (int i = 0; i < word.length(); i++){
                for (int j = 0; j < Vowels.getVoewlsLength(); j++){
                    if(word.charAt(i) == Vowels.getVoewlAt(j))
                        count++;
                }
            }
        }
        return count;
    }
}

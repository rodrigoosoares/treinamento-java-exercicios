package exs1.ex2.classes;

import exs1.ex1.classesauxiliares.Parameters;
import exs1.ex1.interfaces.ICounter;

import java.util.List;

public class NumbersCounter implements ICounter {
    @Override
    public int count(Parameters params) {
        int count =0;
        List<String> words = params.getWords();

        for (String word : words){
            word = word.replaceAll("[a-zA-Z]", "");
            count += word.length();
        }
        return count;
    }
}

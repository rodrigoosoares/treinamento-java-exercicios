package exs1.ex2.classes;

import exs1.ex1.classesauxiliares.Parameters;
import exs1.ex2.classesauxiliares.IndividualParameter;
import exs1.ex2.interfaces.ICounterIndividual;

import java.util.ArrayList;
import java.util.List;

public class LetterCounter implements ICounterIndividual {

    @Override
    public List<IndividualParameter> count(Parameters params) {
        int count; char letter; String wordConcat;
        StringBuilder sb = new StringBuilder();
        List<IndividualParameter> counts = new ArrayList<>();
        List<String> words = params.getWords();

        for (String word : words){ //Cria uma string com todas as palavras
            sb.append(word.replaceAll("[^a-zA-Z]", ""));
        }
        wordConcat = sb.toString().toLowerCase();

        for (int i = 0; i < wordConcat.length(); i++) {//Verifica a ocorrencia e adiciona na lista
            letter = wordConcat.charAt(i);
            count=0;
            for (int j = 0; j < wordConcat.length(); j++) {
                if(wordConcat.charAt(j) == letter)
                    count++;
            }
            wordConcat = wordConcat.replaceAll(Character.toString(letter), "");
            counts.add(new IndividualParameter(letter, count));
        }
        return counts;
    }
}

//Interace para o Execute

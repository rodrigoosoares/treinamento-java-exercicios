package exs1.ex6;

import exs1.ex6.classesauxiliares.Game;
import exs1.ex6.classesauxiliares.Input;
import exs1.ex6.interfaces.IGame;
import exs1.ex6.velha.JogoVelha;

public class Main {
    public static void main(String[] args){
        System.out.println("Bem-vindo ao jogo da velha!\n");

        String piece = new Input().inputPiece();

        Game game = new Game(new JogoVelha());
        game.play(piece);

    }
}

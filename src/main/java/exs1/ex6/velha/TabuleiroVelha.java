package exs1.ex6.velha;

import exs1.ex6.interfaces.ITabuleiro;

public class TabuleiroVelha implements ITabuleiro {

    private String[][] board;

    public TabuleiroVelha() {
        this.board = new String[][] {{"-","-","-"}, {"-","-","-"}, {"-","-","-"} };
    }

    public void playPiece(int r, int c, String piece){
        board[r][c] = piece;
    }

    public String[][] getBoard() {
        return board;
    }

    @Override
    public void showBoard() {/*
        for (int i = 0; i < board.length + 1; i++) {
            for (int j = 0; j < board[i].length + 1; j++) {
                if(i == 0 && j == 0)
                    System.out.print("  ");
                else if (i == 0 || j == 0){
                    if(i != 0)
                        System.out.print(i-1 + " ");
                    else
                        System.out.print(j-1 + " ");
                    }
                else
                    System.out.print(board[i][j-1] + " ");
            }
            System.out.println(" ");
        }*/
        System.out.println("  0 1 2");
        System.out.println("0 "+ board[0][0] + " " + board[0][1] + " " + board[0][2]);
        System.out.println("1 "+ board[1][0] + " " + board[1][1] + " " + board[1][2]);
        System.out.println("2 "+ board[2][0] + " " + board[2][1] + " " + board[2][2]);
    }

    public boolean checkPlay(String[] coordinates) {
        return board[Integer.parseInt(coordinates[0])][Integer.parseInt(coordinates[1])].equals("-");
    }
}

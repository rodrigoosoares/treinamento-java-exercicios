package exs1.ex6.velha;

import exs1.ex6.classesauxiliares.Player;
import exs1.ex6.interfaces.IGame;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class JogoVelha implements IGame {

    private TabuleiroVelha board;

    @Override
    public void play(List<Player> players) {
        Player player1 = players.get(0);
        Player player2 = players.get(1);
        board = new TabuleiroVelha();
        String[] coordinates;
        int round =1;

        board.showBoard();
        while(true){
            if(round==9) {
                System.out.println("Deu Velha!");
                break;
            }

            coordinates = makeAPlay(player1);
            board.showBoard();
            if(checkWinner(coordinates, player1)){
                System.out.println("Parabens Jogador 1!");
                break;
            }

            coordinates = makeAPlay(player2);
            board.showBoard();
            if(checkWinner(coordinates, player2)){
                System.out.println("Parabens Jogador 2!");
                break;
            }
            round++;
        }
    }

    public List<Player> instancePlayers(String piece){
        Player player1 = new Player(piece.toUpperCase());
        Player player2 = new Player();
        List<Player> players = new ArrayList<>();
        if(piece.equals("O"))
            player2.setPiece("X");
        else
            player2.setPiece("O");

        player1.setName("Jogador 1");
        player2.setName("Jogador 2");

        players.add(player1);
        players.add(player2);

        return players;
    }

    private String[] makeAPlay(Player player){
        Scanner sc = new Scanner(System.in);
        String[] coordinates;
        while(true) {
            System.out.println("Faça sua jogada " + player);
            coordinates = splitCoordinates(sc.nextLine());
            if(Integer.parseInt(coordinates[0]) > 2 || Integer.parseInt(coordinates[1]) > 2 ||
                    Integer.parseInt(coordinates[0]) < 0 || Integer.parseInt(coordinates[1]) < 0){
                System.out.println("Coordenadas inválidas. Digite novamente!");
            }else {
                if(board.checkPlay(coordinates)) {
                    board.playPiece(Integer.parseInt(coordinates[0]), Integer.parseInt(coordinates[1]), player.getPiece());
                    break;
                }else
                    System.out.println("Já existe uma peça nessa posição.");
            }
        }
        return coordinates;
    }

    private boolean checkWinner(String[] coordinates, Player player){
        int row = Integer.parseInt(coordinates[0]), column = Integer.parseInt(coordinates[1]);
        int countRow=0, countColumn = 0;
        for (int i = 0; i < 3; i++) {  //Se for na linha
            if(board.getBoard()[row][i].equals(player.getPiece()))
                countRow++;
            if(board.getBoard()[i][column].equals(player.getPiece()))
                countColumn++;
            if(board.getBoard()[0][2].equals(player.getPiece()) && board.getBoard()[1][1].equals(player.getPiece())
                    && board.getBoard()[2][0].equals(player.getPiece())) //Diagonal direita
                return true;
            if(board.getBoard()[0][0].equals(player.getPiece()) && board.getBoard()[1][1].equals(player.getPiece())
                    && board.getBoard()[2][2].equals(player.getPiece())) // diagonal esquerda
                return true;
            if(countColumn == 3 || countRow == 3)
                return true;
        }
        return false;
    }

    public String[] splitCoordinates(String coordinates){
        coordinates = coordinates.replaceAll("[^0-9]", "");
        return new String[]{Character.toString(coordinates.charAt(0)), Character.toString(coordinates.charAt(1))};
    }
}

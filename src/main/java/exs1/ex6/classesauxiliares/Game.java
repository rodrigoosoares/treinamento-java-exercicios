package exs1.ex6.classesauxiliares;

import exs1.ex6.interfaces.IGame;

import java.util.List;

public class Game {

    private IGame game;

    public Game(IGame game) {
        this.game = game;
    }

    public void play(String piece) {
        List<Player> players = game.instancePlayers(piece);
        game.play(players);
    }
}

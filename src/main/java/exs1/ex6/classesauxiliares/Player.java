package exs1.ex6.classesauxiliares;

public class Player {
    private static String[] pieces = {"O", "X"};
    private String piece; // X or O
    private String name;

    public Player(String piece) {
        this.piece = piece;
    }

    public Player() {}

    public String getPiece() {
        return piece;
    }

    public void setPiece(String piece) {
        this.piece = piece;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return this.name;
    }
}

package exs1.ex6.classesauxiliares;

import java.util.Scanner;

public class Input {

    public String inputPiece(){
        Scanner sc = new Scanner(System.in);
        String piece;
        boolean validation = true;
        do {
            if(!validation)
                System.out.println("Escolha inválida. Por favor, escolha 'O' ou 'X'");
            System.out.println("Jogador 1, excolha sua peça ('O' ou 'X')");
            piece = sc.nextLine();
            validation = piece.compareToIgnoreCase("O") == 0 || piece.compareToIgnoreCase("X") == 0;
        }while(!validation);

        return piece;
    }

}

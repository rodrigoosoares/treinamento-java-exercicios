package exs1.ex6.interfaces;

import exs1.ex6.classesauxiliares.Player;

import java.util.List;

public interface IGame {

    void play(List<Player> players);

    List<Player> instancePlayers(String piece);
}
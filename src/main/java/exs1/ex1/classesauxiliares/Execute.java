package exs1.ex1.classesauxiliares;

import exs1.ex1.interfaces.ICounter;

public class Execute {

    private ICounter counter;

    public Execute(ICounter counter) {
        this.counter = counter;
    }

    public void execute(Parameters parms){
        int res = counter.count(parms);
        System.out.println(counter + "/ " + "Count = " + res);
    }
}

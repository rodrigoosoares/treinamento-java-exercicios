package exs1.ex1.classesauxiliares;

import exs1.ex1.interfaces.IInputs;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class WordInput implements IInputs {

    public WordInput() {
    }


    @Override
    public List<String> input() {
        String word = "";
        List<String> words = new ArrayList<>();
        Scanner sc = new Scanner(System.in);

        System.out.println("Contador de caracteres (Digite 'exit' para sair)");

        while(true){
            System.out.print("Digite uma palavra / texto: ");
            word = sc.nextLine().trim();
            if(!word.equalsIgnoreCase("exit"))
                words.add(word);
            else
                break;
        }
        return words;
    }
}

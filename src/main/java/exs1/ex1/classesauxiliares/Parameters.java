package exs1.ex1.classesauxiliares;

import java.util.List;

public class Parameters {

    private List<String> words;

    public Parameters(List<String> words) {
        this.words = words;
    }

    public List<String> getWords() {
        return words;
    }

    public void setWords(List<String> words) {
        this.words = words;
    }
}

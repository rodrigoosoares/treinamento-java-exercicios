package exs1.ex1;

import exs1.ex1.classesauxiliares.Parameters;
import exs1.ex1.interfaces.ICounter;

public class CharCounter implements ICounter {

    @Override
    public int count(Parameters params) {
        int count = 0;
        for (String word : params.getWords()){
            word = word.replaceAll("[^a-zA-Z]", "");
            count += word.length();
        }
        return count;
    }
}

package exs1.ex1.interfaces;

import exs1.ex1.classesauxiliares.Parameters;

public interface ICounter {
    int count(Parameters params);
}

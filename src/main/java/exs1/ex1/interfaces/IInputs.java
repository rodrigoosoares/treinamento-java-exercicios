package exs1.ex1.interfaces;

public interface IInputs<T> {
    T input();
}

package exs1.ex1;

import exs1.ex1.classesauxiliares.Execute;
import exs1.ex1.classesauxiliares.Parameters;
import exs1.ex1.classesauxiliares.WordInput;

public class Main {

    public static void main(String[]  args){
        WordInput wordInput = new WordInput();

        Execute execute = new Execute(new CharCounter());
        execute.execute(new Parameters(wordInput.input()));
    }
}

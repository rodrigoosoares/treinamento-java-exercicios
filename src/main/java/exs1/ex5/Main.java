package exs1.ex5;


import exs1.ex5.classesauxiliares.*;
import exs1.ex5.interfaces.IOperation;

public class Main {
    public static void main(String[] args){
        String expression = new Input().input();
        Spliter spliter = new Spliter();
        String operationToCalculate;

        while(true) {
            Executer executer = new Executer(spliter.splitOperation(expression));
            String[] splitedParameters = spliter.splitParameters(expression);
            Parameter params = new Parameter(Float.parseFloat(splitedParameters[0]), Float.parseFloat(splitedParameters[1]));
            executer.executeToPrint(params);
        }
    }
}
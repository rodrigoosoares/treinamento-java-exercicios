package exs1.ex5.operations;

import exs1.ex5.classesauxiliares.Parameter;
import exs1.ex5.interfaces.IOperation;

public class Potentiation implements IOperation {
    @Override
    public float calcula(Parameter param) {
        return (float) Math.pow(param.getNum1(), param.getNum2());
    }

    public static int getPower(){return 3;}
}

package exs1.ex5.classesauxiliares;

import exs1.ex5.interfaces.IOperation;

public class Executer {

    private IOperation operation;

    public Executer(IOperation operation) {
        this.operation = operation;
    }

    public float execute(Parameter params){
        return operation.calcula(params);
    }

    public void executeToPrint(Parameter params){
        float res = operation.calcula(params);
        System.out.println(res);
    }
}

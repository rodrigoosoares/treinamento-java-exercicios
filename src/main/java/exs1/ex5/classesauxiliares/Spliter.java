package exs1.ex5.classesauxiliares;

import exs1.ex5.interfaces.IOperation;
import exs1.ex5.operations.*;
import exs1.ex5.operations.Module;

import java.util.List;

public class Spliter {

    public String[] splitExpression(String expression){
        // 1 * 8 + 7

        return null;
    }

    public String[] splitParameters(String operation){ // Dividir a operação em parametros
        return operation.split("[\\-+*/%^]");
    }

    public IOperation splitOperation(String operation){ // Retornar qual operação deverá ser feita
        if(operation.contains("+"))
            return new Sum();
        else if(operation.contains("-"))
            return new Subtraction();
        else if(operation.contains("*"))
            return new Multiplication();
        else if(operation.contains("/"))
            return new Division();
        else if(operation.contains("%"))
            return new Module();
        else if(operation.contains("^"))
            return new Potentiation();
        else return null;
    }

    public boolean checkQuantOperations(String expression){ //Verifica se tem mais de uma operação
        int count = 0;
        if(expression.contains("+"))
            count++;
        if(expression.contains("-"))
            count++;
        if(expression.contains("*"))
            count++;
        if(expression.contains("/"))
            count++;
        if(expression.contains("%"))
            count++;
        if(expression.contains("^"))
            count++;
        return count > 1;
    }

    private String getFirstOperation(String expression){
        if(expression.contains("^"))
            return "^";
        else if(expression.contains("%"))
            return "%";
        else if(expression.contains("/"))
            return "/";
        else if(expression.contains("*"))
            return "*";
        else if(expression.contains("-"))
            return "-";
        else if(expression.contains("+"))
            return "+";
        else return null;
    }

    public String findOperationToCalculate(String expression) {
        // 1 + 8 * 8

        return "oi";
    }
}

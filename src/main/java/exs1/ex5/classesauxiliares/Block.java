package exs1.ex5.classesauxiliares;

import exs1.ex5.interfaces.IOperation;

public class Block {
    private String part1; //Primeira conta
    private String part2; // Resto da expressão

    public Block(String part1, String part2) {
        this.part1 = part1;
        this.part2 = part2;
    }

    public Block(String part1) {
        this.part1 = part1;
        this.part2 = "";
    }

    public Block() {
        part1 = "(1 + 8)";
        part2 = "* 9 + 10";
    }

    public String getPart1() {
        return part1;
    }

    public void setPart1(String part1) {
        this.part1 = part1;
    }

    public String getPart2() {
        return part2;
    }

    public void setPart2(String part2) {
        this.part2 = part2;
    }

    public Parameter convertPartToParameter(){
        Spliter spliter = new Spliter();
        String[] parametersOperation;

        part1 = part1.replaceAll("[ ()]", "");
        parametersOperation = spliter.splitParameters(part1);

        return new Parameter(Float.parseFloat(parametersOperation[0]), Float.parseFloat(parametersOperation[1]));
    }

    public IOperation getExpressionOperation(){
        Spliter spliter = new Spliter();
        return spliter.splitOperation(part1);
    }
}

package exs1.ex5.classesauxiliares;

import java.util.Scanner;

public class Input {

    public String input(){
        Scanner sc = new Scanner(System.in);
        System.out.print("Digite a operação: ");
        return sc.nextLine();
    }
}

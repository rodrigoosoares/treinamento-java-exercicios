package exs1.ex5.interfaces;

import exs1.ex5.classesauxiliares.Parameter;

public interface IOperation {

    float calcula(Parameter param);
}
